---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

Hey! I'm Ken, an excited and outgoing developer/student.
I love to do anything related to deep learning and compilers. I can write in Python, Go, HTML, React, and more.
