package main

import (
    "bytes"
    "errors"
    "io/ioutil"
    "os"
    "fmt"
    "path"
    "strings"
    "path/filepath"
    "log"

    "github.com/yuin/goldmark"
    "github.com/yuin/goldmark/extension"
    "github.com/yuin/goldmark/parser"
    "github.com/yuin/goldmark/renderer/html"
    "github.com/yuin/goldmark-emoji"
)

var template = `{%% extends "layout.html" %%}
{%% block heading %%}%s{%% endblock %%}
{%% block subheading %%}%s{%% endblock %%}
{%% block content %%}
<div class="w3-cell w3-mobile" id="about">
<div class="w3-quarter"></div>
<div class="w3-animate-bottom w3-white w3-card-4 w3-round-large w3-padding w3-margin w3-rest">
%s
</div>
<div class="w3-quarter"></div>
</div>
{%% endblock %%}`

func convFile(inPath string, md goldmark.Markdown) error {
    src, err := ioutil.ReadFile(inPath)
    var buf bytes.Buffer
    if err := md.Convert(src, &buf); err != nil {
        return err
    }
    ext := path.Ext(inPath)
    if ext != ".md" {
        return errors.New("file does not end with .md")
    }
    outPath := inPath[0:len(inPath)-len(ext)] + ".html"
    outSrc := buf.String()
    basePath := strings.ReplaceAll(strings.ReplaceAll(filepath.Base(inPath), "_", " "), "-", " ")
    heading := strings.Title(strings.ToLower(basePath[0:len(basePath)-len(ext)]))
    subheading := `Generated using <a href="https://gitlab.com/colourdelete/colourdelete.gitlab.io/-/blob/master/render.go">render.go</a>.`
    err = ioutil.WriteFile(outPath, []byte(fmt.Sprintf(template, heading, subheading, outSrc)), 0644)
    return err
}

func IsFile(path string) (bool, error) {
    fi, err := os.Stat(path)
    if err != nil {
        return false, err
    }
    switch mode := fi.Mode(); {
    case mode.IsDir():
        return false, nil
    case mode.IsRegular():
        return true, nil
    }
    return false, nil
}

var md = goldmark.New(
    goldmark.WithExtensions(
        extension.GFM,
        extension.DefinitionList,
        extension.Footnote,
        extension.Typographer,
        emoji.Emoji,
    ),
    goldmark.WithParserOptions(
        parser.WithAutoHeadingID(),
        parser.WithAttribute(),
    ),
    goldmark.WithRendererOptions(
        html.WithHardWraps(),
        html.WithXHTML(),
        html.WithUnsafe(), // its my own website, doesn't rlly matter and I want to put raw HTML in
    ),
)

func main() {
    log.Println("render - program to render add .md files in current directory recursively. licensing coming soon.")
    err := filepath.Walk(".",
        func(path string, info os.FileInfo, err error) error {
            isFile, err := IsFile(path)
            if err != nil {
                log.Println(path, err)
                return err
            }
            switch isFile {
            case true:
                err = convFile(path, md)
                log.Println(path, "file", err)
            case false:
                log.Println(path, "dir")
            }
            return nil
        },
    )
    if err != nil {
        log.Fatalln(err)
    }
}
