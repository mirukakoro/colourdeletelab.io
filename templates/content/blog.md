# Blog

Hi, here's my "blog", which mostly contain reposts from Medium or other stuff.

#### [Differences between AI, ML, DL, and What They Are](/ai-ml-dl-explain)

Posted on Medium: 2020-10-30
Reposted on here: 2020-11-17

When you hear something with “AI”, what comes to mind? Although most are referring to deep learning, a new technique to train neural networks to make decisions (e.g. is this a cat or a dog?), AI is a broad field, and there is much more to it. Let’s explore the what the differences and similarities between AI, ML, DL, and what they are.

#### [How To Make a New Programming Language `1.1`](/toy-lang)

Posted on Medium: 2020-10-09
Reposted on here: 2020-11-17

Hot to make a new, simple toy programming language and a compiler for it. This toy language is a good step to start learning about and implementing compilers, parsers, and programming languages.
```
> out(add(1,2))
3
```

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTc1MTUwMTcyNV19
-->