# Jotai Concept

Jotai is a service that lets you manage your presence on many platforms, such as Slack, Discord, GitHub, and GitLab.

You can use Jotai to control the presence such as online and offline, and status message and emoji.

Jotai

> [Personal] Discord (myhandlename#1234)
> ✅🟢 Online < click to change
> ✅🏝️ Vacation! < status message

^ Discord supports both online/offline and status message

> [Personal/Business] GitLab (gitlab.com, johndoe)
> ✅🔴 Offline
> ❎🏝️ Vacation!

^ GitLab only supports online/offline or status message; clicking one enables it and disables the other

> [Personal] Slack (Personal Slack Workspace, myhandlename)
> ✅🟢 Offline
> ❎🏝️ Vacation!

> [Business] Slack (Acme Inc, johndoe
> ✅🔴 Offline
> ❎🏝️ Vacation!

<!--stackedit_data:
eyJoaXN0b3J5IjpbOTg0MTI4OTY0LDIxMzAwOTQ0NCwxNzk0ND
Q2OTY5LDI3OTYyNDY5NiwyMTM4NTAxNDg0LC0xMzQxODI5OTkx
LC0xNjk3MTMzODU0XX0=
-->