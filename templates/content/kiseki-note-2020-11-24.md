---
Name: "Kiseki Note 2020-11-24"
Author: Ken Shibata
---
# Kiseki Note `2020-11-24`

[Jamboard](https://jamboard.google.com/d/1J5QNAeLmWzUceTGeABUZZmIsXzI6Fy-IQjsJqjNaqTw/viewer)

- couple of frameworks maybe used w/ data [^data]
- Options
	- feed forward network
		- general linear model
			- `import scipy.stats.glm as glm`
		    - `a = glm.fit(your_position_data, your_score)`
	    - multi layer perceptron
			- `from scipy import perceptron`
- Automated data collection
	- random or systematic init ball pos and init ball impulse
	- save to `data.csv` [^data]

[^data]: [Kiseki Data - Google Sheets](https://docs.google.com/spreadsheets/d/1S0UakUGXARFdntWJ5yDLRkJnRnPdzfgQ-Pnys6_BVJM/edit#gid=0)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4Mjk0MTAwNDQsNzI2MDU2NjddfQ==
-->