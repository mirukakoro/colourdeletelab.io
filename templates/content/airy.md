# Airy

## Outline

### Model View Controller

Airy uses the "Model View Controller" architecture. Each Model is a directory with a config file, `airy-config.toml`, the model logic and assets. A Model directory also doubles as a Python package, and the `__init__.py` will contain code with functions/methods such as `new_view` to create new Views from a Model. Main model logic is encouraged to be stored in `__main__.py`. Each asset group (made of 1 or more asset files) will have it's own config file. Add a `.` before a file to make sure it is ignored. A file native to the 3D/2D game engine may be generated (eg `.bam` and/or `.egg` for Panda3D)

- `car/`
	- `airy-model.toml`
	- `__init__.py`
	- `__main__.py`
	- `.ignored-file.txt`
	- `assets/`
		- `airy-assets.toml`
		- `wheel/`
			- `airy-asset.toml`
			- `wheel.fbx`
			- `wheel.bam`
			- `wheel.egg`
		- `body/`
			- `airy-asset.toml`
			- `body.fbx`
			- `body.bam`
			- `body.egg`

#### `car/airy-model.toml`

```toml
[meta]
name = "Car"
desc = "A simple generic car model."
author = "Ken Shibata"

# more fields may be added later
```

#### `car/__init__.py`

```python3
import airy

model = airy.Model()
model.speaker = airy.Speaker()
```

#### `car/__main__.py`

```python3
import airy

model = airy.Model.get()

@model.when(airy.events.model.move)
def model_event_move(model: airy.Model, event: airy.Event) -> airy.Status:
	model.speaker.play(model.assets.get('engine_rev'))
	return airy.Status.OK
```

#### `car/assets/airy-assets.toml`

```toml
[meta]
name = "Car Assets"
desc = "Assets for a simple generic car model."
author = "Ken Shibata"

# more fields may be added later
```

#### `car/assets/wheel/airt-asset.toml`

```toml
[meta]
name = "Car Wheel"
desc = "Wheel for a simple generic car model."
author = "Ken Shibata"

[asset.3d]
files = [ "wheel.fbx" ]

[asset."wheel.fbx".size]
x = "200mm"
y = "600mm"
z = "600mm"

[asset."wheel.fbx".pos]
x = 0 # centered
y = 0 # integer values are interpreted with mm units
z = 0

[asset."wheel.fbx".rot]
x = 0
y = 0
z = 0

# more fields may be added later
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbODA1OTQyMTc5LDYzMTI3NDY2MywzNjEzOD
cyNjAsMTAyNDY4MzE4NywtMTcxMDAxODYzMywtMTkzMjk5ODQw
OF19
-->